/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { DRAG_FORMAT_MEDIUM } from "@/constants/drag-and-drop";
import Input from "@/elements/Input/Input.vue";
import Thumbnail from "@/elements/Thumbnail/Thumbnail.vue";
import Window from "@/elements/Window/Window.vue";
import { _SET_MEDIA_LIBRARY_VISIBILITY } from "@/store/modules/editor/constants";
import { IMedium } from "@/store/modules/editor/types/medium";
import durationFormat from "@/utils/duration-format";
import Vue from "vue";
import { mapMutations, mapState } from "vuex";

export default Vue.extend({
  components: {
    Input,
    Thumbnail,
    Window,
  },

  data() {
    return {
      searchText: "",
    };
  },

  computed: {
    ...mapState("editor/mediaLibrary", ["media"]),
  },

  methods: {
    ...mapMutations("editor", [_SET_MEDIA_LIBRARY_VISIBILITY]),

    hideMediaLibrary() {
      this[_SET_MEDIA_LIBRARY_VISIBILITY]({newVisibility: false});
    },

    mediumFormattedLength(medium: IMedium) {
      return durationFormat(medium.lengthMs);
    },

    mediumEligible(medium: IMedium) {
      return medium.name.toLowerCase().includes(this.searchText.toLowerCase());
    },

    handleMediumItemDragStart(medium: IMedium, event: any) {
      event.dataTransfer.setData(DRAG_FORMAT_MEDIUM, medium.id);
    },
  },
});
