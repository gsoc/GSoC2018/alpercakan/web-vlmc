/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import Button from "@/elements/Button/Button.vue";
import SeekBar from "@/elements/SeekBar/SeekBar.vue";
import Window from "@/elements/Window/Window.vue";
import { _SET_CLIP_PREVIEW_VISIBILITY,
         _SET_PROJECT_PREVIEW_VISIBILITY,
         SET_CURRENT_CLIP_TIME } from "@/store/modules/editor/constants";
import { REGISTER_SEEK_LISTENER, SET_CURRENT_WORKFLOW_TIME } from "@/store/modules/editor/modules/workflow/constants";
import durationFormat from "@/utils/duration-format";
import Vue from "vue";
import Slider from "vue-slider-component/src/vue2-slider.vue";
import { mapGetters, mapMutations, mapState } from "vuex";

const DEFAULT_FPS = 25;

export default Vue.extend({
  components: {
    Button,
    SeekBar,
    Slider,
    Window,
  },

  props: {
    clipMode: { // allows markers and cutting
      default: false,
      type: Boolean,
    },
  },

  data() {
    return {
      canPlay: false,
      duration: 0,
      isPlaying: false,
      markerAPos: -1,
      markerBPos: -1,
      soundLevel: 100,
    };
  },

  created() {
    if (!this.clipMode) {
      this.$store.commit(
        REGISTER_SEEK_LISTENER,
        { seekListener: this.seekTo });
    }
  },

  computed: {
    ...mapState("editor",
                  ["clipPlaybackUrl",
                   "projectPlaybackUrl"]),
    ...mapGetters("editor/workflow", ["selectedClip"]),
    ...mapGetters("editor/mediaLibrary", ["getMediumById"]),

    title() {
      return this.clipMode ? "Clip Preview" : "Project Preview";
    },

    url() {
      return this.clipMode ? this.clipPlaybackUrl : this.projectPlaybackUrl;
    },

    currentTimeMs() {
      return this.clipMode ? this.$store.state.editor.clipCurrentTimeMs :
                             this.$store.state.editor.workflow.currentTimeMs;
    },

    formattedTime() {
      return durationFormat(this.currentTimeMs, true);
    },

    fps() {
      if (this.clipMode) {
        const clipParent = this.selectedClip ? this.getMediumById(this.selectedClip.id) : null;

        return clipParent ? clipParent.fps : DEFAULT_FPS;
      } else {
        return this.$store.state.editor.workflow.fps;
      }
    },
  },

  methods: {
    ...mapMutations("editor", [_SET_CLIP_PREVIEW_VISIBILITY, _SET_PROJECT_PREVIEW_VISIBILITY]),

    hidePreview() {
      this[this.clipMode ? _SET_CLIP_PREVIEW_VISIBILITY : _SET_PROJECT_PREVIEW_VISIBILITY]
        ({newVisibility: false});
    },

    setCurrentTime(newTime: number) {
      this.$store.commit(
        this.clipMode ? SET_CURRENT_CLIP_TIME : SET_CURRENT_WORKFLOW_TIME,
        { newTime, skipListener: true });
    },

    play() {
      if (!this.canPlay) {
        return;
      }

      this.isPlaying = true;
      this.$refs.video.play();
    },

    pause() {
      this.isPlaying = false;
      this.$refs.video.pause();
    },

    stop() {
      this.pause();
      this.$refs.video.currentTime = 0;
      this.setCurrentTime(0);
    },

    seekTo(to: number) {
      this.$refs.video.currentTime = to / 1000;
    },

    // Video control events
    handlePrevFrameButtonClick() {
      this.pause();

      this.seekTo(this.currentTimeMs - 1000 / this.fps);
    },

    handlePlayPauseButtonClick() {
      if (this.isPlaying) {
        this.pause();
      } else {
        this.play();
      }
    },

    handleNextFrameButtonClick() {
      this.pause();

      this.seekTo(this.currentTimeMs + 1000 / this.fps);
    },

    handleStopButtonClick() {
      this.stop();
    },

    handleSetMarkerClick(left: boolean) {
      if (left) {
        this.markerAPos = this.currentTimeMs;
      } else {
        this.markerBPos = this.currentTimeMs;
      }

      if (this.markerAPos > this.markerBPos &&
          this.markerBPos >= 0) {
        if (left) {
          this.markerBPos = -1;
        } else {
          this.markerAPos = -1;
        }
      }
    },

    handleCutClick() {
      // TODO
    },

    handleSoundLevelChange() {
      this.$refs.video.volume = this.soundLevel / 100;
    },

    // Playback events
    handleCanPlayEvent() {
      this.canPlay = true;
    },

    handleVideoTimeUpdate() {
      if (!this.$refs.video) {
        /*
         * Since this event is so frequent, this method can be run just
         * after <video> is removed from DOM.
         */
        return;
      }

      this.setCurrentTime(this.$refs.video.currentTime * 1000);
    },

    handleVideoDurationChange() {
      this.durationMs = this.$refs.video.duration * 1000;
    },

    handleVideoEnded() {
      this.stop();
    },

    handleSeekClick(e: any) {
      this.seekTo(e.to);
    },
  },
});
