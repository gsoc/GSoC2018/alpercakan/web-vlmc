/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import api from "@/api";
import Controller from "@/store/modules/editor/controller";
import { IRootState } from "@/store/types";
import { ActionTree } from "vuex";
import { _SET_AVAILABLES_EFFECTS_LIST,
         _SET_AVAILABLES_TRANSITIONS_LIST,
         _SET_ERROR,
         _SET_LOADING } from "./constants";
import { MessageType } from "./controller/types/message-type";
import { IEditorState } from "./types/editor-state";
import { IMedium } from "./types/medium";
import { ITransition } from "./types/transition";

const wait = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const actions: ActionTree<IEditorState, IRootState> = {
  connect({ commit, dispatch }) {
    const handleError = () => commit(_SET_ERROR, { error: true });

    Controller.registerMessageListener(MessageType.LengthChangedEvent,
      (value: { length: number }) => commit("workflow/setLength", { length: value.length }));

    api.post("/instance")
    .then(({ data }) => {
      /* XXX
       * Waiting for a constant amount time is not okay. However, for now,
       * due to a problem in parent-child process communucation in Python,
       * we cannot establish a proper communucation between Maestro and the
       * VLMC instance. Hence, Maestro just does not know when the instance
       * is ready for incoming socket connections.
       */
      return  wait(3000).then(() => { Controller.connect(data.socketAddress,
                                data.authToken,
                                handleError,
                                handleError); }).then(() => wait(5000));
    })
    .then(() => {
      Promise.all(
        [
          dispatch("fetchTransitionsList"),
          dispatch("fetchEffectsList"),
          dispatch("fetchMediaLibrary"),
          dispatch("fetchClipLibrary").then(() => dispatch("fetchWorkspace")),
          dispatch("fetchProjectSettings"),
        ])
        .then(() => {
          commit(_SET_LOADING, {
            loading: false,
          });
        });
    })
    .catch(() => {
      handleError();
    });
  },

  fetchTransitionsList({ commit }) {
    return Controller.getTransitionsList()
    .then((value: { transitions: ITransition[] }) => {
      commit(_SET_AVAILABLES_TRANSITIONS_LIST, {
        list: value.transitions,
      });
    });
  },

  fetchEffectsList({ commit }) {
    return Controller.getEffectsList()
    .then((value: { effects: ITransition[] }) => {
      commit(_SET_AVAILABLES_EFFECTS_LIST, {
        list: value.effects,
      });
    });
  },

  fetchMediaLibrary({ commit }) {
    return Controller.getMediaLibrary()
    .then((value: { media: IMedium[] }) => {
      commit("mediaLibrary/setMedia", {
        media: value.media,
      });
    });
  },

  fetchWorkspace({ commit }) {
    return Controller.getWorkspace()
    .then((value: any) => {
      commit("workflow/setTracks", { tracks: value.tracks });
      commit("workflow/setClipInstances", { clips: value.clips });
      commit("workflow/setLength", { length: value.workflowLength });
    });
  },

  fetchClipLibrary({ commit }) {
    return Controller.getClipLibrary()
    .then((value: any) => {
      commit("workflow/setClips", { clips: value.clipLibrary });
    });
  },

  fetchProjectSettings({ commit }) {
    return Controller.getProjectSettings()
    .then((value: any) => {
      commit("workflow/setFps", { fps: value.fps });
    });
  },
};

export default actions;
