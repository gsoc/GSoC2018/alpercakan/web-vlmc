/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import Controller from "@/store/modules/editor/controller";
import { MutationTree } from "vuex";
import { _SET_AVAILABLES_EFFECTS_LIST,
         _SET_AVAILABLES_TRANSITIONS_LIST,
         _SET_CLIP_LIBRARY_VISIBILITY,
         _SET_CLIP_PREVIEW_VISIBILITY,
         _SET_CURRENT_CLIP_TIME,
         _SET_EFFECTS_LIST_VISIBILITY,
         _SET_ERROR,
         _SET_LOADING,
         _SET_MEDIA_LIBRARY_VISIBILITY,
         _SET_PROJECT_PREVIEW_VISIBILITY,
         _SET_TRANSITIONS_LIST_VISIBILITY } from "./constants";
import { IEditorState } from "./types/editor-state";
import { IEffect } from "./types/effect";
import { ITransition } from "./types/transition";

const mutations: MutationTree<IEditorState> = {
  [_SET_MEDIA_LIBRARY_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.mediaLibraryVisible = payload.newVisibility;
  },

  [_SET_CLIP_LIBRARY_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.clipLibraryVisible = payload.newVisibility;
  },

  [_SET_CLIP_PREVIEW_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.clipPreviewVisible = payload.newVisibility;
  },

  [_SET_PROJECT_PREVIEW_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.projectPreviewVisible = payload.newVisibility;
  },

  [_SET_EFFECTS_LIST_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.effectsListVisible = payload.newVisibility;
  },

  [_SET_TRANSITIONS_LIST_VISIBILITY](state, payload: { newVisibility: boolean }) {
    state.transitionsListVisible = payload.newVisibility;
  },

  [_SET_CURRENT_CLIP_TIME](state, payload: { newTime: number }) {
    state.clipCurrentTimeMs = payload.newTime;
  },

  [_SET_LOADING](state, payload: { loading: boolean }) {
    state.loading = payload.loading;
  },

  [_SET_ERROR](state, payload: { error: boolean }) {
    state.error = payload.error;
  },

  [_SET_AVAILABLES_TRANSITIONS_LIST](state, payload: { list: ITransition[] }) {
    state.availableTransitions = payload.list;
  },

  [_SET_AVAILABLES_EFFECTS_LIST](state, payload: { list: IEffect[] }) {
    state.availableEffects = payload.list;
  },
};

export default mutations;
