/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import api from "@/api";
import { InputType } from "@/store/modules/editor//types/input-type";
import Controller from "@/store/modules/editor/controller";
import { IRootState } from "@/store/types";
import * as uuid from "uuid/v4";
import { ActionTree } from "vuex";
import { _ADD_CLIP_INSTANCE, _ADD_EFFECT_INSTANCE, _REMOVE_CLIP_INSTANCE } from "./constants";
import { IWorkflowState } from "./types/workflow-state";

const actions: ActionTree<IWorkflowState, IRootState> = {
  [_ADD_EFFECT_INSTANCE]({ commit }, payload) {
    Controller.addEffect(payload.clipId, payload.effectName).then((newEffectInstance) => {
      commit("addEffectInstance", {
        effectInstance: {
          ...newEffectInstance,
          targetType: InputType.CLIP,
        },
      });
    });
  },

  [_ADD_CLIP_INSTANCE]({ commit }, payload: { isProvisional: boolean, clipId: string, trackId: string, pos: number }) {
    const trackIdWithoutFormat = payload.trackId.substr(0, payload.trackId.length - 1);

    Controller.addClip(payload.clipId, trackIdWithoutFormat, payload.pos)
    .then((data) => {
        data.clips.forEach((instance: any) => {
          commit(_ADD_CLIP_INSTANCE, {
            clipInstance:
              {
                clipId: payload.clipId,
                containingTrackId: `${instance.trackId}${instance.isAudio ? "a" : "v"}`,
                id: instance.id,
                position: payload.pos,
              } });
        });
      });
  },

  [_REMOVE_CLIP_INSTANCE]({ commit }, payload) {
    Controller.removeClip(payload.id).then(() => {
      commit("removeClipInstance", {
        id: payload.id,
      });
    });
  },
};

export default actions;
