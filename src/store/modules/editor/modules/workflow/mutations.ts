/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { IClip } from "@/store/modules/editor/types/clip";
import { IClipInstance } from "@/store/modules/editor/types/clip-instance";
import { IEffectInstance } from "@/store/modules/editor/types/effect-instance";
import { ITrack } from "@/store/modules/editor/types/track";
import * as uuid from "uuid/v4";
import { MutationTree } from "vuex";
import { InputFormat } from "../../types/input-format";
import { _ADD_CLIP_INSTANCE, _ADD_EFFECT_INSTANCE,
         _REGISTER_SEEK_LISTENER, _REMOVE_CLIP_INSTANCE, _SET_CLIP_INSTANCES,
         _SET_CLIPS, _SET_CURRENT_WORKFLOW_TIME, _SET_FPS, _SET_LENGTH,
         _SET_TRACKS } from "./constants";
import { IWorkflowState } from "./types/workflow-state";

const mutations: MutationTree<IWorkflowState> = {
  [_SET_CURRENT_WORKFLOW_TIME](state, payload: { newTime: number, skipListener: boolean }) {
    state.currentTimeMs = payload.newTime;

    if (!payload.skipListener && state.seekListener) {
     state.seekListener(payload.newTime);
    }
  },

  [_REGISTER_SEEK_LISTENER](state, payload: { seekListener: (_: number) => void }) {
    state.seekListener = payload.seekListener;
  },

  [_SET_TRACKS](state, payload: { tracks: any[] }) {
    const tracks: ITrack[] = payload.tracks.map((t) =>
      (t.type ? { id: `${t.id}a`, format: InputFormat.AUDIO } : { id: `${t.id}v`, format: InputFormat.VIDEO }));

    state.videoTracks = tracks.filter((t) => t.format === InputFormat.VIDEO);
    state.audioTracks = tracks.filter((t) => t.format === InputFormat.AUDIO);
  },

  [_SET_CLIP_INSTANCES](state, payload: { clips: any[] }) {
    state.clipInstances = payload.clips.map((clip) => ({
      clipId: clip.clipUuid,
      containingTrackId: `${clip.trackId}${clip.isAudio ? "a" : "v"}`,
      id: clip.uuid,
      position: clip.position,
    }));
  },

  [_SET_FPS](state, payload: { fps: number }) {
    state.fps = payload.fps;
  },

  [_SET_CLIPS](state, payload: { clips: IClip[] }) {
    state.clips = payload.clips;
  },

  [_SET_LENGTH](state, payload: { length: number }) {
    state.length = payload.length;
  },

  [_ADD_EFFECT_INSTANCE](state, payload: { effectInstance: IEffectInstance }) {
    state.effectsIntances.push(payload.effectInstance);
  },

  [_ADD_CLIP_INSTANCE](state, payload: { clipInstance: IClipInstance }) {
    state.clipInstances.push(payload.clipInstance);
  },

  [_REMOVE_CLIP_INSTANCE](state, payload: { id: string }) {
    state.clipInstances = state.clipInstances.filter((c) => c.id !== payload.id);
  },
};

export default mutations;
