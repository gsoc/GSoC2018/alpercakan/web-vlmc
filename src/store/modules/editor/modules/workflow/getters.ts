/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { IClip } from "@/store/modules/editor/types/clip";
import { IClipInstance } from "@/store/modules/editor/types/clip-instance";
import { IEffectInstance } from "@/store/modules/editor/types/effect-instance";
import { ITrack } from "@/store/modules/editor/types/track";
import { IRootState } from "@/store/types";
import { GetterTree } from "vuex";
import { IWorkflowState } from "./types/workflow-state";

const getters: GetterTree<IWorkflowState, IRootState> = {
  selectedClip(state): IClip | null {
    return state.clips.find((clip: IClip) => clip.id === state.selectedClipId) || null;
  },

  getClipInstancesOnTrack(state): ((track: ITrack) => IClipInstance[]) {
    return (track: ITrack) =>
    state.clipInstances.filter((clipInstance: IClipInstance) => clipInstance.containingTrackId === track.id);
  },

  getClipFromInstance(state): ((instance: IClipInstance) => IClip | null) {
    return (instance: IClipInstance) =>
    state.clips.find((clip: IClip) => clip.id === instance.clipId) || null;
  },

  getEffectsOnClip(state): ((clip: IClipInstance) => IEffectInstance[]) {
    return (clip: IClipInstance) => state.effectsIntances.filter((e: IEffectInstance) => e.targetId === clip.id);
  },
};

export default getters;
