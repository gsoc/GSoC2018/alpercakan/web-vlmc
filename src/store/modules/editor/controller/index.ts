/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import * as uuid from "uuid/v4";
import { ICallbackPair } from "./types/callback-pair";
import { CommandType } from "./types/command-type";
import { MessageType } from "./types/message-type";
import { Reject } from "./types/reject";
import { Resolve } from "./types/resolve";

class Controller {
  public static getInstance() {
    return Controller.instance || (Controller.instance = new Controller());
  }

  private static instance: Controller;

  private static serialize(data: object) {
    return JSON.stringify(data);
  }

  private static deserialize(str: string) {
    return JSON.parse(str);
  }

  private static wrapInPromise(fn: (resolve: Resolve, reject: Reject) => void) {
    return new Promise(fn);
  }

  private static getResponseType(requestType: MessageType): MessageType {
    return requestType + 1;
  }

  private static validateMessage(message: any) {
    return true;
  }

  private socket: WebSocket | null;
  private authDone: boolean;
  private authToken: string | null;
  private messageIndex: number;
  private callbackQueue: Array<{[key: string]: ICallbackPair}>;
  private readyCallbacks: ICallbackPair | null;
  private registeredListeners: Array<Array<{onSuccess?: (value?: any) => void,
                                            onError?: (reason?: any) => void}>>;
  private onError: (() => void) | null;
  private onClosed: (() => void) | null;

  private constructor() {
    this.socket = null;
    this.authDone = false;
    this.authToken = null;
    this.messageIndex = 0;
    this.readyCallbacks = null;
    this.onError = null;
    this.onClosed = null;

    this.callbackQueue = [];
    this.registeredListeners = [];

    for (let type = 0; type < MessageType.NbMessageType; ++type) {
      this.callbackQueue[type] = { };
      this.registeredListeners[type] = [];
    }

    this.bindFunctions();
  }

  public connect(serverUrl: string,
                 authToken: string,
                 onError: () => void,
                 onClosed: () => void) {
    this.authToken = authToken;
    this.onError = onError;
    this.onClosed = onClosed;

    const socket = this.socket = new WebSocket(serverUrl);

    socket.addEventListener("open", this.handleSocketReady);
    socket.addEventListener("error", this.handleSocketError);
    socket.addEventListener("message", this.handleNewSocketMessage);
    socket.addEventListener("close", this.handleSocketClosed);

    return Controller.wrapInPromise((resolve, reject) => {
      this.readyCallbacks = { resolve, reject };
    });
  }

  public addEffect(clipId: string, effectName: string) {
    return this.send({ payload: { clipId, effectName },
                type: CommandType.ADD_EFFECT },
              MessageType.AddEffectRequest);
  }

  public addClip(clipId: string, trackId: string, pos: number) {
    return this.send({ payload: { clipId, trackId, pos },
                       type: CommandType.ADD_CLIP},
                       MessageType.AddClipRequest);
  }

  public removeClip(id: string) {
    return this.send({ payload: { id },
      type: CommandType.REMOVE_CLIP},
      MessageType.RemoveClipRequest);
  }

  public getProjectSettings() {
    return this.send({ }, MessageType.ProjectSettingsRequest);
  }

  public getTransitionsList() {
    return this.send({ }, MessageType.TransitionsListRequest);
  }

  public getEffectsList() {
    return this.send({ }, MessageType.EffectsListRequest);
  }

  public getMediaLibrary() {
    return this.send({ }, MessageType.MediaLibraryRefreshRequest);
  }

  public getWorkspace() {
    return this.send({ }, MessageType.WorkspaceRequest);
  }

  public getClipLibrary() {
    return this.send({ }, MessageType.ClipLibraryRequest);
  }

  /**
   * Registers message listeners to be called when a message of the given type
   * arrives.
   *
   * The difference between the Promise-based methods and this one is that the
   * callback registered using this method will be called on arrival of each
   * message of the given type, until they are unregistered.
   *
   * Note that, when they do exist, the methods resolve/reject of the returned
   * Promise from a call to a request method will be called before the methods
   * onSuccess/onError of any registered message listener.
   *
   * @param type Message type for which the callbacks will be registered
   * @param onSuccess The function to be called when the message is successful
   * @param onError The function to be called when the message is not successful
   */
  public registerMessageListener(type: MessageType,
                                 onSuccess?: (value?: any) => void,
                                 onError?: (reason?: any) => void) {
    this.registeredListeners[type].push({ onSuccess, onError });
  }

  private bindFunctions() {
    this.handleSocketReady = this.handleSocketReady.bind(this);
    this.handleSocketError = this.handleSocketError.bind(this);
    this.handleNewSocketMessage = this.handleNewSocketMessage.bind(this);
    this.handleSocketClosed = this.handleSocketClosed.bind(this);
    this.dequeueCallbacks = this.dequeueCallbacks.bind(this);
  }

  private enqueueCallbacks(pair: ICallbackPair,
                           type: MessageType,
                           correspondenceId: string) {
    this.callbackQueue[type][correspondenceId] = pair;
  }

  private dequeueCallbacks(type: MessageType,
                           correspondenceId: string) {
    const callbacks = this.callbackQueue[type][correspondenceId];

    delete this.callbackQueue[type][correspondenceId];

    return callbacks;
  }

  private tryAuth() {
    this.send({ token: this.authToken }, MessageType.AuthRequest)
    .then(() => {
      this.authDone = true;

      if (this.readyCallbacks) {
        this.readyCallbacks.resolve();
      }
    }).catch(() => {
      // TODO handle auth fail
      console.log("AUTH ERROR");

      if (this.readyCallbacks) {
        this.readyCallbacks.reject();
      }
    });
  }

  private handleSocketReady() {
    if (this.authDone) {
      return;
    }

    this.tryAuth();
  }

  private handleSocketError() {
    if (this.onError) {
      this.onError();
    }
  }

  private handleSocketClosed() {
    if (this.onClosed) {
      this.onClosed();
    }
  }

  private handleNewSocketMessage(event: MessageEvent) {
    const message = Controller.deserialize(event.data);

    if (!Controller.validateMessage(message)) {
      return; // TODO handle fail
    }

    const callbacks = this.dequeueCallbacks(message.type, message.correspondenceId);
    const listeners = this.registeredListeners[message.type];

    if (callbacks) {
      const { resolve, reject } = callbacks;

      if (message.success === false) {
        reject();
      } else {
        resolve(message.content);
      }
    }

    for (const { onSuccess, onError } of listeners) {
      if (message.success === false) {
        if (onError) { onError(); }
      } else {
        if (onSuccess) { onSuccess(message.content); }
      }
    }
  }

  private send(content: object, type: MessageType) {
    return Controller.wrapInPromise((resolve, reject) => {
      const correspondenceId = uuid();

      this.enqueueCallbacks({ resolve, reject },
                            Controller.getResponseType(type),
                            correspondenceId);

      if (!this.socket) {
        return;
      }

      this.socket.send(Controller.serialize(
        {
          content,
          correspondenceId,
          index: this.messageIndex++,
          timestamp: new Date(),
          type,
        }));
    });
  }
}

const controller = Controller.getInstance();

export default controller;
