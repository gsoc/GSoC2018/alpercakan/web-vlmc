/*****************************************************************************
 * Copyright (C) 2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { IRootState } from "@/store/types";
import { Module } from "vuex";
import actions from "./actions";
import getters from "./getters";
import mediaLibrary from "./modules/media-library";
import workflow from "./modules/workflow";
import mutations from "./mutations";
import { IEditorState } from "./types/editor-state";

const state = {
  clipPlaybackUrl: "http://localhost:8089/The.Good.Place.S02E01.720p.HDTV.x264-KILLERS.mkv", // XXX MOCK DATA
  projectPlaybackUrl: "http://localhost:8089/b2.mkv",

  clipCurrentTimeMs: 0,

  clipLibraryVisible: true,
  clipPreviewVisible: true,
  effectsListVisible: true,
  mediaLibraryVisible: true,
  projectPreviewVisible: true,
  transitionsListVisible: true,

  availableEffects: [],
  availableTransitions: [],

  error: false,
  loading: true,
};

const editor: Module<IEditorState, IRootState> = {
  actions,
  getters,
  modules: {
    mediaLibrary,
    workflow,
  },
  mutations,
  namespaced: true,
  state,
};

export default editor;
