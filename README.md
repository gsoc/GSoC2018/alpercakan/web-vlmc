# WebVLMC

WebVLMC is the web interface for VideoLAN Movie Creator. It is a free and open source software distributed under the terms of the [GPLv2].

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn start

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report

# run unit tests
yarn unit

# run e2e tests
yarn e2e

# run all tests
yarn test

# lint
yarn lint

# lint autofix
yarn lint:fix
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## How to Use?
Get VLMC and VLMC-Maestro. After building VLMC in server mode, place the executable inside the same folder as the Maestro. Start Maestro (refer to its readme file for details) and then start WebVLMC.

[VLMC]: https://code.videolan.org/videolan/vlmc
[GPLv2]: <https://opensource.org/licenses/GPL-2.0>
[freenode]: <http://www.freenode.net/>
[Freenode Web]: <http://webchat.freenode.net/>
[how to send patches]: <https://wiki.videolan.org/Sending_Patches_VLC/>
[Development mailing-list]: https://mailman.videolan.org/listinfo/vlmc-devel
